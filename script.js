NOT_FOUND = -9;

isOp = function (x) {
    return new RegExp("&|\\||=|<|>|\\+|\\-|\\*|/|\\^|\\[|\\]|\\{|\\}|\\(|\\)|,| ").test(x);
}

isSep = function (x) {
    return new RegExp("_").test(x);
}

wasFound = function(x) {
    return x != NOT_FOUND;
}

notFound = function(x) {
    return x == NOT_FOUND;
}


identifyString = function (inputArea) {

    expr = inputArea.value;

    if (expr == "") {
        return null;
    }

    cursorPosition = getCursorPos(inputArea);
    opBefore = -1;
    nSepsBefore = 0;
    opAfter = NOT_FOUND;
    sepBefore = NOT_FOUND;
    sepAfter = NOT_FOUND;

    i = 0;
    while (opAfter == NOT_FOUND && sepAfter == NOT_FOUND) {
        if (i == expr.length) {
            opAfter = i;
            break
        }
        // c = the character
        c = expr.substr(i, 1);
        if (i < cursorPosition.start) {
            if (isOp(c)) {
                opBefore = i;
                nSepsBefore = 0;
                sepBefore = NOT_FOUND;
                sepAfter = NOT_FOUND;
            }
            if (isSep(c)) {
                nSepsBefore++;
                sepBefore = i;
            }
        }
        if (i >= cursorPosition.start) {
            if (isOp(c)) {
                opAfter = i;
            }
            if (isSep(c)) {
                sepAfter = i;
            }
        }
        //console.log("i=" + i + ", opBefore=" + opBefore +  ", opAfter=" + opAfter +  ", sepBefore=" + sepBefore +  ", sepAfter=" + sepAfter + ", cursorPos=" + cursorPosition.start);

        i++;
    }
    //console.log("i=" + i + ", opBefore=" + opBefore + ", opAfter=" + opAfter + ", sepBefore=" + sepBefore + ", sepAfter=" + sepAfter + ", nSepsBefore=" + nSepsBefore + ", cursorPos=" + cursorPosition.start);

    replaceFrom = Math.max(opBefore, sepBefore) + 1;
    
    replaceTo = 0;
    if (opAfter != NOT_FOUND) {
        replaceTo = opAfter - 1;
    }
    if (sepAfter != NOT_FOUND) {
        replaceTo = sepAfter - 1;
    }

    replaceLength = replaceTo - replaceFrom + 1;

    type = "unknown";
    if (replaceLength > 0) {
        if (notFound(sepBefore) && notFound(sepAfter)) {
            type = "kontext_oder_merkmalsart";
        } else if (notFound(sepBefore) && wasFound(sepAfter)) {
            type = "kontext";
        } else if (wasFound(sepBefore) && wasFound(sepAfter)) {
            type = "spalten_merkmalsart";
        } else if (wasFound(sepBefore) && notFound(sepAfter)) {
            if (nSepsBefore == 1) {
                type = "spalten_merkmalsart";
            } else if (nSepsBefore == 2) {
                type = "zeilen_merkmalsart";
            }
        }
    }

    document.getElementById("txtOutput").value = (
        "Extracted string: " + expr.substr(replaceFrom, replaceLength) + "\n" +
        "Type: " + type + "\n" +
        "Replace from: " + replaceFrom + "\n" +
        "Replace to: " + replaceTo + "\n" +
        "Replace length: " + replaceLength + "\n"
    );

    return {
        string: expr.substr(replaceFrom, replaceLength),
        type: type,
        replaceFrom: replaceFrom,
        replaceTo: replaceTo,
        replaceLength: replaceLength
    };
}

// From: https://stackoverflow.com/a/7745998/1553796
function getCursorPos(input) {
    if ("selectionStart" in input && document.activeElement == input) {
        return {
            start: input.selectionStart,
            end: input.selectionEnd
        };
    }
    else if (input.createTextRange) {
        var sel = document.selection.createRange();
        if (sel.parentElement() === input) {
            var rng = input.createTextRange();
            rng.moveToBookmark(sel.getBookmark());
            for (var len = 0;
                     rng.compareEndPoints("EndToStart", rng) > 0;
                     rng.moveEnd("character", -1)) {
                len++;
            }
            rng.setEndPoint("StartToStart", input.createTextRange());
            for (var pos = { start: 0, end: len };
                     rng.compareEndPoints("EndToStart", rng) > 0;
                     rng.moveEnd("character", -1)) {
                pos.start++;
                pos.end++;
            }
            return pos;
        }
    }
    return -1;
}