# Autosuggestion for SAIMo
This repository does not actually implement an autosuggestion feature. Instead, it extracts from a given mathematical expression the relevant string that should be autocompleted.

## Mode of operatoin
Given the syntax for "Merkmale" as "KONTEXT_SPALTE_ZEILE", the algorithm figures out if the curser is momentarily located in the KONTEXT, SPALTE or ZEILE. Given it is possible to call a Merkmalsart (either SPALTE or ZEILE) directly by its name when operating in the same dimension (e.g. writing an expression for a Zeilen-Merkmalsart), in some cases it is not possible to figure out whether the user is trying to type a KONTEXT, a SPALTE or a ZEILE. In this case, the ambiguity is indicated.

## Examples
In the examples, the cursor position is indicated with the `|` sign.


**Example 1 (ambiguous: KONTEXT or MERKMALSART)**
```
abc_efg_hij + mno_pqr_stu + vwx|x
```
Result
```
Extracted string: vwxx
Type: kontext_oder_merkmalsart
Replace from: 28
Replace to: 31
Replace length: 4
```

**Example 2 (inside of KONTEXT)**
```
abc_efg_hij + m|no_pqr_stu + vwxx
```
Result
```
Extracted string: mno
Type: kontext
Replace from: 14
Replace to: 16
Replace length: 3
```

**Example 3 (inside of SPALTE)**
```
abc_efg_hij + mno_|pqr_stu + vwxx
```
Result
```
Extracted string: pqr
Type: spalten_merkmalsart
Replace from: 18
Replace to: 20
Replace length: 3
```

**Example 4 (inside of ZEILE)**
```
abc_efg_hij + mno_pqr_stu| + vwxx
```
Result
```
Extracted string: stu
Type: zeilen_merkmalsart
Replace from: 22
Replace to: 24
Replace length: 3
```

**Example 5 (outside of KONTEXT, SPALTE or ZEILE)**
```
abc_efg_hij + mno_pqr_stu |+ vwxx
```
Result
```
Extracted string: 
Type: unknown
Replace from: 26
Replace to: 25
Replace length: 0
```

